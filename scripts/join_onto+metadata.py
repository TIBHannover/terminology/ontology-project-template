# Join the ontology file and the metadata file into a new turtle file

from pathlib import Path
from rdflib import Graph

# Create a Graph
g_metadata = Graph()
g_onto = Graph()

# Define metadata and ontology paths
onto_path = Path().absolute() / 'Ontology.owl' # path of the ontology
metadata_path = Path().absolute() / 'metadata.owl' # path of ontology metadata

# Parse graphs
g_onto.parse(str(onto_path), format="xml") # parse Ontology.owl
g_metadata.parse(str(metadata_path), format="xml") # parse metadata.owl 

# Print the number of "triples" in the Graph
# Prints length of graphs created from Ontology.owl and metadata.owl
print(f"Graph g_metadata generated from {metadata_path} has {len(g_metadata)} statements.")
print(f"Graph g_onto generated from {metadata_path} has {len(g_onto)} statements.")

# Join both graphs (g_metadata, g_onto) into a joint graph (g_joint)
g_joint = Graph()
g_joint = g_metadata + g_onto
print(f"Graph g_joint has {len(g_joint)} statements.") # Print length of the joint graph

# Serialize the new graph into a new file
# Define path for serialization of joint graphs:
joint_graph_serialization = Path().absolute() / 'myOnto.ttl'
print(f"Graphs g_metadata and g_onto will be joint and serialized into new file {joint_graph_serialization}.")
# Create joint onto file
g_joint.serialize(str(joint_graph_serialization), format="ttl")
# Ontology Project Template

**This is a draft!**

This is a template for ontology projects.

It helps you to get started with your own project and helps you to make your ontology FAIR by supporting you in

- performing quality checks,
- creating an ontology documentation with Widoco,
- providing rich metadata for your ontology.

## How to reuse

You can use this project as a template for your own ontology project.

- Clone this repo on GitLab
- Update metadata.owl
- Use Ontology.owl to develop your ontology
- Update Readme.md to introduce your ontology
- Update the [widoco_sections](widoco_sections) for your ontology's online documentation


## More info on...

### Ontology documentation with Widoco

The CI-Pipeline defined in [.gitlab-ci.yml](.gitlab-ci.yml) will generate a human-readable online documentation of the ontology from the ontology itself. It uses the software [WIzard for DOCumenting Ontologies (WIDOCO)](https://github.com/dgarijo/Widoco). The documentation can be customized via:
- adding ontology metadata to [config.properties](config.properties) - these will mainly be used for the header of the documentation
- customizing the sections of the documentation in [widoco_sections](widoco_sections)

There is [A checklist for complete vocabulary metadata](https://dgarijo.github.io/Widoco/doc/bestPractices/index-en.html). Widoco uses the proposed properties when generating the documentation.

Check out the documentation that has been generated for [myOnto.ttl](myOnto.ttl) at [https://tibhannover.gitlab.io/terminology/ontology-project-template](https://tibhannover.gitlab.io/terminology/ontology-project-template). 

## License

<a rel="license" href="https://creativecommons.org/publicdomain/zero/1.0/legalcode"><img alt="CC0" style="border-width:0" width=88 src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/cc-zero.png" /></a><br />The Ontology Project Template is dedicated to the <a rel="license" href="https://creativecommons.org/publicdomain/zero/1.0/legalcode">public domain</a>.

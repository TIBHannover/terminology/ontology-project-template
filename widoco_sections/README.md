This directory contains sections for your ontologys's online documentation. The content of the files will be used to replace some generic text used by Widoco.

Here is an overview of what is expected in each section:

|section|expected content|
|-|-|
|Abstract|The abstract should contain a couple of sentences summarizing the ontology and its purpose.|
|1.Introduction|The introduction should briefly describe the ontology, its motivation, state of the art and goals.|
|3.Description|The description should include an explanation and a diagram explaining how the classes are related, examples of usage, etc.|
|5.References|Add your references here. It is recommended to have them as a list.|